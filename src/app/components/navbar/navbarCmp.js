import angular from 'angular'

import navbarTpl from './navbarTpl.html'

class NavbarCtrl {
    constructor() {
        this.icon = '/img/logo.png'
    }
}

let navbarCmp = {
    name: 'navbar',
    bindings: {},
    controller: NavbarCtrl,
    template: navbarTpl
}

export default angular.module(navbarCmp.name, [])
    .component(navbarCmp.name, navbarCmp)
    .name