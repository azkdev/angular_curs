import angular from 'angular'
import firebase from 'firebase'

class FireService {

    constructor() {
        this.configs = {
            apiKey: 'AIzaSyC8HjGTwAoMvz1fMzPOadzJcRkybu8PGkE',
            authDomain: 'ang-fire-azkdev.firebaseapp.com',
            databaseUrl: 'https://ang-fire-azkdev.firebaseio.com',
            projectId: "ang-fire-azkdev",
            storageBucket: 'ang-fire-azkdev.appspot.com',
            messagingSenderId: "297852903408"
        }
    }

    initializeFirebase() {
        firebase.initializeApp(this.configs)
        return firebase
    }

}

export default angular.module('services.firebase-service', [])
    .service('firebaseService', FireService)
    .name