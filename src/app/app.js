import angular from 'angular'

// Essential imports for angular app.
import uiRouter from '@uirouter/angularjs'
import translate from 'angular-translate'
import aria from 'angular-aria'
import angularfire from 'angularfire'

// Firebase configs.
import firebaseService from './configs/firebase/firebase.config'

// Vendor styles.
import 'font-awesome/css/font-awesome.min.css'
import 'bulma/css/bulma.css'

// Personal styles
import '../style/app.css'

// Template for app-base component.
import appTpl from './app.html'

// Imports for own components.
import navbar from './components/navbar/navbarCmp'


// Base app controller.
class AppCtrl {
	constructor() {}
}

// Assemble our "app" component.
let appCmp = {
	name: 'app',
	controller: AppCtrl,
	template: appTpl
}

// Expose app component for use with tagname "<app></app>"
export default angular.module(appCmp.name, [
		uiRouter,
		translate,
		aria,
		angularfire,
		firebaseService,
		navbar
	])
	.component(appCmp.name, appCmp)
	.name